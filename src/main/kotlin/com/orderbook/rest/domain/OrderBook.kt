package com.orderbook.rest.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*
import javax.validation.constraints.NotNull


@Entity
@Table(name = "order_book")
data class OrderBook (
    @Id
    //@JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int? = 0,

    @get: NotNull
    @Column(name = "kode_saham", nullable = false)
    var kodeSaham: String? = " ",

    @get: NotNull
    @Column(name = "harga_terakhir")
    var hargaTerakhir: Int = 0,

    @get: NotNull
    @Column(name = "change")
    var change: Int? = 0,

    @get: NotNull
    @Column(name = "close")
    var close: Int? = 0,

    @get: NotNull
    @Column(name = "open")
    var open: Int? = 0,

    @get: NotNull
    @Column(name = "high")
    var high: Int? = 0,

    @get: NotNull
    @Column(name = "low")
    var low: Int? = 0,

    @get: NotNull
    @Column(name = "foreign_sale")
    var foreignSale: Int? = 0,

    @get: NotNull
    @Column(name = "order_bid")
    var orderBid: Int? = 0,

    @get: NotNull
    @Column(name = "lot_bid")
    var lotBid: Int? = 0,

    @get: NotNull
    @Column(name = "domestic_buy")
    var domesticBuy: Int? = 0,

    @get: NotNull
    @Column(name = "bid_offer")
    var bidOffer: Int? = 0,

    @get: NotNull
    @Column(name = "lot_offer")
    var lotOffer: Int? = 0
)