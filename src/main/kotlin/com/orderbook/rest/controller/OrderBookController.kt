package com.orderbook.rest.controller

import com.orderbook.rest.domain.OrderBook
import com.orderbook.rest.repository.OrderBookRepository
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = arrayOf("http://localhost:8080"))
class OrderBookController(
    private val orderBookRepository: OrderBookRepository
) {

    @GetMapping("/order-books")
    fun getAllOrderBook(): List<OrderBook> =
        orderBookRepository.findAll()

    @GetMapping("/order-books/{id}")
    fun getOrderBook(@PathVariable id: Int): Optional<OrderBook> {
        return orderBookRepository.findById(id)
    }

    @PostMapping("/order-books")
    fun saveOrderBook(@RequestBody orderBook: OrderBook): OrderBook {
        return orderBookRepository.save(orderBook)
    }

    @PutMapping("/order-books/{id}")
    fun updateOrderBookById(
        @PathVariable(value = "id") orderBookId: Int,
        @Valid @RequestBody newOrderBook: OrderBook
    ): ResponseEntity<OrderBook> {
        return orderBookRepository.findById(orderBookId).map { OrderBook ->
            val update: OrderBook = OrderBook
                .copy(
                    kodeSaham = newOrderBook.kodeSaham,
                    hargaTerakhir = newOrderBook.hargaTerakhir,
                    change = newOrderBook.change,
                    close = newOrderBook.close,
                    open = newOrderBook.open,
                    high = newOrderBook.high,
                    low = newOrderBook.low,
                    foreignSale = newOrderBook.foreignSale,
                    orderBid = newOrderBook.orderBid,
                    lotBid = newOrderBook.lotBid,
                    domesticBuy = newOrderBook.domesticBuy,
                    bidOffer = newOrderBook.bidOffer,
                    lotOffer = newOrderBook.lotOffer
                )
            ResponseEntity.ok().body(orderBookRepository.save(update))
        }.orElse(ResponseEntity.notFound().build())

    }

    @DeleteMapping("/order-books/{id}")
    fun deleteOrderBook(@PathVariable(value = "id") orderBook: Int): Optional<Unit>? {
        return orderBookRepository.findById(orderBook).map { OrderBook ->
            orderBookRepository.delete(OrderBook)
        }
    }
}