package com.orderbook.rest.repository

import com.orderbook.rest.domain.OrderBook
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface OrderBookRepository : JpaRepository<OrderBook, Int>{


}
