package com.orderbook.rest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoOrderBookApplication

fun main(args: Array<String>) {
	runApplication<DemoOrderBookApplication>(*args)
}
